%if 0%{?rhel} && ! 0%{?epel}
%bcond_with ansible
%else
%bcond_without ansible
%endif

%bcond_with collection_artifact

%global collection_namespace_orig fedora
%global collection_name_orig linux_mgmt

%if 0%{?rhel}
%global collection_namespace redhat
%global collection_name rhel_mgmt
%else
%global collection_namespace %{collection_namespace_orig}
%global collection_name %{collection_name_orig}
%endif

Name:           ansible-collection-%{collection_namespace}-%{collection_name}
Url:            https://github.com/pcahyna/fedora.linux_mgmt/
Summary:        Ansible Collection of general system management and utility modules and other plugins
Version:        1.0.0
Release:        2%{?dist}

License:        GPLv3+

%global collection_version %{version}
%global archivename %{collection_namespace_orig}.%{collection_name_orig}-%{version}
%global extractdir %{archivename}

# Helper macros originally from macros.ansible by Igor Raits <ignatenkobrain>
# Not available on RHEL, so we must define those macros locally here without using ansible-galaxy

# Not used (yet). Could be made to point to AH in RHEL - but what about CentOS Stream?
#%%{!?ansible_collection_url:%%define ansible_collection_url() https://galaxy.ansible.com/%%{collection_namespace}/%%{collection_name}}

%{!?ansible_collection_files:%define ansible_collection_files %{_datadir}/ansible/collections/ansible_collections/%{collection_namespace}}

%if %{with ansible}
BuildRequires: ansible >= 2.9.10
Requires: ansible >= 2.9.10
%endif

%if %{undefined ansible_collection_build}
%if %{without ansible}
# We don't have ansible-galaxy.
%define ansible_collection_build() tar -cf %{_tmppath}/%{collection_namespace}-%{collection_name}-%{version}.tar.gz .
%else
%define ansible_collection_build() ansible-galaxy collection build
%endif
%endif

%if %{undefined ansible_collection_install}
%if %{without ansible}
# Simply copy everything instead of galaxy-installing the built artifact.
%define ansible_collection_install() mkdir -p %{buildroot}%{ansible_collection_files}/%{collection_name}; (cd %{buildroot}%{ansible_collection_files}/%{collection_name}; tar -xf %{_tmppath}/%{collection_namespace}-%{collection_name}-%{version}.tar.gz)
%else
%define ansible_collection_install() ansible-galaxy collection install -n -p %{buildroot}%{_datadir}/ansible/collections %{collection_namespace}-%{collection_name}-%{version}.tar.gz
%endif
%endif

Source: https://github.com/pcahyna/fedora.linux_mgmt/archive/%{version}/%{archivename}.tar.gz

BuildArch: noarch

BuildRequires: python3
BuildRequires: python3dist(ruamel.yaml)

# utility for build
# originally from: https://github.com/linux-system-roles/auto-maintenance
# rev. 20d31bf5d8e7eb67ce48af39e36c9f79d87490e3
# MIT license: https://github.com/linux-system-roles/auto-maintenance/blob/master/LICENSE
Source1: galaxy_transform.py

%if %{undefined __ansible_provides}
Provides: ansible-collection(%{collection_namespace}.%{collection_name}) = %{collection_version}
%endif

%description
%{summary}.
Targeted at GNU/Linux systems.

%if %{with collection_artifact}
%package collection-artifact
Summary: Collection artifact to import to Automation Hub / Ansible Galaxy

%description collection-artifact
Collection artifact for %{name}. This package contains %{collection_namespace}-%{collection_name}-%{version}.tar.gz
%endif

%prep
%autosetup -n %{extractdir}

# Replacing original (Galaxy) collection name by downstream (Automation Hub) name
%if "%{collection_namespace_orig}" != "%{collection_namespace}" || "%{collection_name_orig}" != "%{collection_name}"
find -type f -exec \
    sed "s/%{collection_namespace_orig}[.]%{collection_name_orig}/%{collection_namespace}.%{collection_name}/g" -i {} \;
%endif

# borrowed from from ansible-collection-ansible-netcommon
find -type f ! -executable -type f -name '*.py' -print -exec sed -i -e '1{\@^#!.*@d}' '{}' +

%{SOURCE1} %{collection_namespace} %{collection_name} %{collection_version} > galaxy.yml.new
mv galaxy.yml.new galaxy.yml

%build
%ansible_collection_build

%install
%ansible_collection_install

%if %{with collection_artifact}
# Copy collection artifact to /usr/share/ansible/collections/ for collection-artifact
if [ -f %{collection_namespace}-%{collection_name}-%{version}.tar.gz ]; then
    mv %{collection_namespace}-%{collection_name}-%{version}.tar.gz \
       $RPM_BUILD_ROOT%{_datadir}/ansible/collections/
fi
%endif

%files
%dir %{_datadir}/ansible
%license COPYING
%{ansible_collection_files}

%if %{with collection_artifact}
%files collection-artifact
%{_datadir}/ansible/collections/%{collection_namespace}-%{collection_name}-%{version}.tar.gz
%endif

%changelog
* Thu Aug 26 2021 Pavel Cahyna <pcahyna@redhat.com> - 1.0.0-2
- Create collection artifact subpackage, disabled by default
  Taken from rhel-system-roles.

* Thu Aug 05 2021 Pavel Cahyna <pcahyna@redhat.com> - 1.0.0-1
- Initial version
